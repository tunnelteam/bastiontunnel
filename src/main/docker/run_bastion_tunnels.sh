#!/usr/bin/env bash

# echo the commands to console so they can be seen in the docker logs
set -x


# -N means "do not execute remote command"

# -D means "dynamic port forwarding" (ie. SOCKS proxy)
# We specify a listen address of  "0.0.0.0" which means to listen on all
# ip addresses, otherwise it will listen on# what it thinks is the machine's
# ip address (likely 172.something), but that IP is mostly private to the
# container and not visible to others.

# -i supply the identity file
# The keypair file is expected to be in a directory that was mapped as a
# data volume from the host machine (NOT committed to the source code).
# Remember that the keyfile must have restrivite permissions or ssh will whine,
# something like "600".

# the two "-o" options skip the whole host fingerprint verification thing

nohup ssh $BASTION_USER@$BASTION_HOST -N \
  -D 0.0.0.0:$SOCKS_PROXY_PORT \
  -i $BASTION_KEYFILE \
  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no &



# -L does direct port forwarding, anything that comes to the
# $DIRECT_FORWARD_PORT will immediately be forwarded to DIRECT_FORWARD_ADDRESS

if [ "$DIRECT_FORWARD_ADDRESS" = "" ]; then
  echo "skipping direct port forward because DIRECT_FORWARD_ADDRESS is not set"
else
  nohup ssh $BASTION_USER@$BASTION_HOST -N \
    -L 0.0.0.0:$DIRECT_FORWARD_PORT:$DIRECT_FORWARD_ADDRESS \
    -i $BASTION_KEYFILE \
    -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no &
fi



# diskCacheRoot - disable file cache
# localDocumentRoot - disable polipo web server

/usr/bin/polipo \
  proxyAddress=0.0.0.0 proxyPort=$HTTP_PROXY_PORT \
  socksParentProxy=localhost:$SOCKS_PROXY_PORT \
  socksProxyType=socks5 \
  diskCacheRoot= \
  localDocumentRoot=

