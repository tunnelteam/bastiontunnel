[TOC]

Description
====

This docker image is intended to help with accessing private web servers
that can only be reached over an ssh bastion (authorised by keypair).
I use it for a standard ssh bastion setup on AWS, but it could be used for
other tunneling needs, I guess.

Runnable docker images are published on DockerHub at
https://hub.docker.com/r/tunnel/bastiontunnel/.

Windows Docker Toolbox usage
====

My primary usage ennvironment are Windows 8 / 10 machines, so I use
Docker Toolbox.

Private key pre-requisite
----

* the private key part of the key pair needs to be available on the Linux
docker host machine (a virtualbox Linux VM if you're using docker-toolbox).
The key file needs to have "600" permissions (requirement of openssh).
In the example below, assume a
"private-keyfile.pem" file exists at "/home/docker/keypairs" in the Linux host
 machine.

Running the tunnels
----

Run something like the following command:
```
docker run \
  -v /home/docker/keypairs:/keypairs \
  -e BASTION_KEYFILE=/keypairs/<private-keyfile.pem> \
  -e BASTION_USER=<bastion-username> \
  -e BASTION_HOST=<bastion.machine.example.com> \
  -p 63000:8000 \
  -p 63001:8080 \
  --name dev-tunnel \
  -d tunnel/bastiontunnel:0.0.1
```
This will give you a SOCKS proxy on port 63000 and a HTTP proxy on port 63001
both of which will be tunneled over your ssh bastion to give you access to
your private web sites.

Those ports will be accessible on the IP address of the docker host, so
probably 192.168.99.100 or similar (use "docker-machine ip" to find it).
After that, you can point your browser to a HTTP proxy at
192.168.99.100:63001 (or port 63000 for a SOCKS proxy, if you'd rather).
I use
[proxy-switchyomega](https://www.google.com.au/search?q=proxy-switchyomega)
so I can limit use of the proxy to only addresses that can only be accessed
over the bastion.

### Direct port forwarding

The bastiontunnel can also do one "direct port forward" .
If you specify a "host:port" value in the environment variable
DIRECT_FORWARD_ADDRESS, the container will forward anything it receives on
port 9000 (which must map onto a port on you machine in the same way as the
proxy ports) to that address, via the bastion machine.

So that'd look something like the following.
```
docker run \
  -v /home/docker/keypairs:/keypairs \
  -e BASTION_KEYFILE=/keypairs/<private-keyfile.pem> \
  -e BASTION_USER=<bastion-username> \
  -e BASTION_HOST=<bastion.machine.example.com> \
  -e DIRECT_FORWARD_ADDRESS=<target-machine.example.com:80> \
  -p 63000:8000 \
  -p 63001:8080 \
  -p 5000:9000 \
  --name dev-tunnel \
  -d tunnel/bastiontunnel:version.0.0.1
```



Details - what's it actually doing?
====

* The container runs a SOCKS proxy on port 8000 using the `ssh -D`
"dynamic tunnel" feature.
* The container runs a HTTP Proxy on port 8080 using [polipo](
https://www.irif.univ-paris-diderot.fr/~jch/software/polipo/).
* Optionally, the container can also run a direct port forward on port 9000,
using the `ssh -L` feature.


You can find the Dockerfile at
[src/main/docker/Dockerfile](./src/main/docker/Dockerfile), but
[src/main/docker/run_bastion_tunnels.sh](
  ./src/main/docker/run_bastion_tunnels.sh),
is likely what you want, that's where the magic happens :P
Make sure you're looking at the correct verison of the code, see the versioning
section for an explanation.  That holds for this readme file as well, you're
probably looking at the very latest readme file, but you may be running an
container with different features/behaviour than that described here.

Ignore the Gradle stuff, I use it for local development because the JVM is
my hammer.



Docker tag / source code versioning
====
Each docker tag like "0.0.0" corresponds directly to the source code with an
annotated git tag of the form "version.0.0.0".
So watch out, if you followed the source repo link from
dockerhub, you're looking at the code from the TIP of the master branch.
The TIP may differ significantly from the image you're actually using, inspect
the code you want by using the bitbucket source view (link on the left), then
use the version widget at the top left to view the exact source for the the
image you're using.

Each docker tag is explicitly created by hand with the intent of being a
"usable" published version.  There may be a "latest" tag created as a
snapshot/floating type version at some point, but I'd advise against use.